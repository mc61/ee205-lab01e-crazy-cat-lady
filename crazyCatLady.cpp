///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  Caleb Mueller <mc61@hawaii.edu>
/// @date    11_01_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

int main(int argc, char *argv[]) {

  std::string name = argv[1];
  std::cout << "Oooooh! " << name << " you're so cute!" << std::endl;

  return 0;
}
